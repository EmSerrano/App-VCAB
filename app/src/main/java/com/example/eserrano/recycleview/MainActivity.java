package com.example.eserrano.recycleview;

import android.app.Dialog;
import android.content.Context;
import android.support.v4.app.FragmentManager;
//import android.support.v4.app.Fragment;
//import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
//import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity
{
    //Se crea una lista porque es la se va a enviar como parametro a la clase adapterDatos.
    ArrayList<String> listDatos;
    RecyclerView recycler;
    String cad = "";
    int num = 0  ;
    Button btnShowDialog;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnShowDialog = (Button) findViewById(R.id.alert);

        recycler = (RecyclerView) findViewById(R.id.MyRecyclerView);
        recycler.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));
        //Este layoutmanager sirve para mostrar la información del reciclerview.
        listDatos = new ArrayList<String>(); //se crea un array oara agregar los datos.

        for(int i = 0;i<=50;i++) // Este for va a representar la información que se cargará en la lista.
        {
            //listDatos.add("Dato # " + i + " \n" ); // Con este ciclo for se llenanrá la lista a mostrar.
                if(i%2==0)
                {
                    listDatos.add("El número "+ i + " es par\n");
                }
                else
                {
                   listDatos.add("El número "+ i + " es impar\n");
                }
        }
        AdapterDatos adapter = new AdapterDatos(listDatos); //Le enviamos la lista para que se haga la referencia.
        //ahora al recycler le mandamos el adaptor.
        recycler.setAdapter(adapter);
        context = this;

        //Evento click para mandar preguntar los parametros pedidos y posteriormente mandar el alertDialog.
        btnShowDialog.setOnClickListener( new View.OnClickListener()
            {
                //android.app.AlertDialog.Builder mensaje = new android.app.AlertDialog.Builder(MainActivity.this);
                //View mview = getLayoutInflater().inflate(R.layout.activity_main,null);
                @Override
                public void onClick(View view)
                {
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    alertprueba dialogo = new alertprueba();
                    Dialog dialog01 = dialogo.OnCreateDialog(context); //cast ()
                    dialog01.show();

                }
            });
    }
    public void comprobar(String cadena, int numero)
    {

        if(cadena == "www.google.com" && numero == 20120330)
        {
            System.out.println("Es correcta la url");

        }
        else
        {
            System.out.println("esta vacia la varible");

        }
    }

}
