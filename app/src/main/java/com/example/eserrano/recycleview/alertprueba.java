package com.example.eserrano.recycleview;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.Toast;

/**
 * Created by E.serrano on 5/8/18.
 */
public class alertprueba extends DialogFragment
{
    String cad = "www.google.com";
    int num = 20120330;


    public Dialog OnCreateDialog(final Context context)
    {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);


        builder.setTitle("Información")
                .setMessage("Esto es un mensaje de alerta.")
                .setCancelable(false)
                .setIcon(R.mipmap.ic_launcher)
                .setPositiveButton("Checar", new DialogInterface.OnClickListener()
                {
                    Toast mensaje;
                    public void onClick(DialogInterface dialog, int id)
                    {
                        System.out.println("Aceptar");
                        MainActivity main = new MainActivity();
                        main.comprobar(cad,num);
                        mensaje = Toast.makeText(context,"Es correcta la url",Toast.LENGTH_LONG);
                        mensaje.show();
                        final AlertDialog mDialog = builder.create();
                        mDialog.setCanceledOnTouchOutside(false);
                        mDialog.show();
                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i)
                    {
                        Toast mensaje;
                        System.out.println("Cancelar");
                        mensaje = Toast.makeText(context,"Es incorrecta la url",Toast.LENGTH_LONG);
                        mensaje.show();
                        setCancelable(false);
                    }
                });


        return builder.create();
    }


}

