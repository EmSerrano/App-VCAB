package com.example.eserrano.recycleview;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by E.serrano on 5/7/18.
 */

public class AdapterDatos extends RecyclerView.Adapter<AdapterDatos.ViewHolderDatos>
{
    ArrayList<String> listDatos;

    public AdapterDatos(ArrayList<String> listDatos)
    //Aquí llegará una lista de datos y se asigna a este elemento "listaDatos"
    //Constructor explicito con la información que le va a llegar de la variable ListDatos.
    {
        this.listDatos= listDatos;
    }

    @Override
    public ViewHolderDatos onCreateViewHolder(ViewGroup parent, int viewType)
    //Este método ayuda a enlazarnos este adaptador con el archivo item_list.xml
    {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list,null,false);
        //Aquí se infla este view y se retorna un nuevo view holder de los datos, ya que estamos esperando la referencia del viewholder
        return new ViewHolderDatos(view);
    }

    @Override
    public void onBindViewHolder(ViewHolderDatos holder, int position)
    {
        //Establece la comunicación entre nuestro adaptador y la clase ViewHolderDatos.
        holder.asignarDatos(listDatos.get(position));
    }

    @Override
    public int getItemCount()
    {
        //Retorna el tamaño de la lista que llega.
        return listDatos.size();
    }

    public class ViewHolderDatos extends RecyclerView.ViewHolder
    {
        TextView dato;
        public ViewHolderDatos(View itemView)
        {
            super(itemView);
            dato = itemView.findViewById(R.id.nombre);
        }

        public void asignarDatos(String datos)
        {
            dato.setText(datos);
        }
    }


}
